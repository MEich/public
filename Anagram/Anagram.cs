﻿using System;
using System.Collections.Generic;

namespace Anagram
{

	public class Anagram
	{
		private string reference;

		public Anagram (string input)
		{
		 	reference = input;
		}

		public object Match(string[] words)
		{
			List<string> anagrams = new List<string>();

			foreach(var word in words)
			{
				if (isAnagram(word))
				{
					anagrams.Add(word);					
				}
			}
			return anagrams;
		}

		public bool isAnagram(string input)
		{
			var referenceAsArray = reference.ToLower().ToCharArray();
			var wordAsArray = input.ToLower().ToCharArray();

			if (isNotSameLength(wordAsArray, referenceAsArray) 
				|| (isIdenticalWord(reference, input)))
			{
				return false;
			}	

			return areArraysEqual(referenceAsArray, wordAsArray);
		}

		public bool isIdenticalWord(string reference, string word)
		{
			return (word.ToLower() == reference.ToLower());
		}

		public bool isNotSameLength(char[] words, char[] reference)
		{
			return (words.Length != reference.Length);
		}

		public bool areArraysEqual(char[] referenceAsArray, char[] wordAsArray)
		{
			Array.Sort<char> (referenceAsArray);
			Array.Sort<char> (wordAsArray);

			EqualityComparer<char> comparer = EqualityComparer<char>.Default;

			for (int i = 0; i < wordAsArray.Length; i++)  
			{
				if (!comparer.Equals(referenceAsArray[i], wordAsArray[i]))
				{
					return false;
				}
			}
			return true;
		}
	}
}