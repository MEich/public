#ifndef FOOD_CHAIN_H
#define FOOD_CHAIN_H

#include <string>
#include <map>

class food_chain
{
  
public:
  static std::string verse(int i);
  static std::string verses(int from, int to);
  static std::string sing();
private:
  static std::string addUniqueLine(std::string outText, int i  );
  static std::string addMiddle(std::string outText, int i );
  static bool isLastVerse(int i);
  static std::string animal(int i);
  static std::string finalVerse(std::string outText, int i );
  static std::string allVerses(std::string outText, int i );
};

#endif


