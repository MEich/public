#include <string>
#include <map>
#include "food_chain.h"
#include <vector>
#include <iostream>

//---------------------------------------------------------------------
std::string food_chain::verse(int i)
//---------------------------------------------------------------------
{
  std::string outText;
  
  outText = "I know an old lady who swallowed a " + animal(i) + ".\n" + allVerses(outText, i);
  outText = finalVerse( outText, i);
  
  return outText;
}
//---------------------------------------------------------------------
std::string food_chain::addUniqueLine(std::string outText, int i )
//---------------------------------------------------------------------
{
  std::vector<std::string> container {
    "It wriggled and jiggled and tickled inside her.\n",
    "How absurd to swallow a bird!\n",
    "Imagine that, to swallow a cat!\n",
    "What a hog, to swallow a dog!\n",
    "Just opened her throat and swallowed a goat!\n",
    "I don't know how she swallowed a cow!\n"
  };
  
  if(i != 1)
  {
    outText += container[i-2];
  }
  return outText;
}

//---------------------------------------------------------------------
std::string food_chain::addMiddle(std::string outText, int i )
//---------------------------------------------------------------------
{
  std::string newlineFullstop = ".\n";
  
  for (int j = i ; j > 1; j--)
  {
    outText += "She swallowed the " + animal(j) + " to catch the " + animal(j-1);
    
    if (j == 3)
    {
      outText += " that wriggled and jiggled and tickled inside her";
    }
    outText += newlineFullstop ;
  }
  return outText;
}

//---------------------------------------------------------------------
bool food_chain::isLastVerse(int i)
//---------------------------------------------------------------------
{
  return (i == 8);
}

//---------------------------------------------------------------------
std::string food_chain::animal(int i)
//---------------------------------------------------------------------
{
  std::map<int, std::string> Namemapping;
  Namemapping[1] = "fly";
  Namemapping[2] = "spider";
  Namemapping[3] = "bird";
  Namemapping[4] = "cat";
  Namemapping[5] = "dog";
  Namemapping[6] = "goat";
  Namemapping[7] = "cow";
  Namemapping[8] = "horse";

  return Namemapping[i];
}

//---------------------------------------------------------------------
std::string food_chain::verses(int from, int to)
//---------------------------------------------------------------------
{
  std::string composition;

  for (int i = from ; i <= to; i ++)
  {
    composition += food_chain::verse(i) + "\n";
  }
  return composition;
}

//---------------------------------------------------------------------
std::string food_chain::sing()
//---------------------------------------------------------------------
{
  return food_chain::verses(1,8);
}

//---------------------------------------------------------------------
std::string food_chain::finalVerse(std::string outText, int i )
//---------------------------------------------------------------------
{
  if (isLastVerse(i))
  {
    std::string lastVerse = "She's dead, of course!\n";
    outText += lastVerse;
  }
  return outText;
}

//---------------------------------------------------------------------
std::string food_chain::allVerses(std::string outText, int i )
//---------------------------------------------------------------------
{
  if(!isLastVerse(i))
  {
    std::string lastLine = "I don't know why she swallowed the fly. Perhaps she'll die.\n";
    outText = addUniqueLine( outText, i );
    outText = addMiddle( outText, i );
    outText += lastLine;
  }
  return outText;
}

