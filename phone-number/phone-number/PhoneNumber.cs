﻿using System;
using System.Text.RegularExpressions;

namespace phonenumber
{
	public class PhoneNumber
	{
		public string Number;
		public string AreaCode;

		public PhoneNumber(string rawNumber)
		{
			Number = extract(rawNumber);
			AreaCode = Number.Substring(0,3);
		}

		private string extract(string Number)
		{	
			return processDigits(getDigits(Number));
		}

		private string processDigits(string Number)
		{
			if (!SameCount(Number, 10))
			{
				return SameCount(Number, 11)?ProcessElevenDigitString(Number):invalidate(Number);
			}
			else return Number;
		}

		private string getDigits( string Number)
		{
			Regex character = new Regex(@"[A-z]");
			Regex digit = new Regex(@"[^\d]");

			if (character.IsMatch(Number))
			{
				Number = invalidate(Number);
			}
			return digit.Replace(Number, "");
		}

		private string ProcessElevenDigitString(string Number)
		{
			return FirstDigitIsOne(Number)?RemoveFirst(Number):invalidate(Number);
		}

		private bool SameCount(string Number, int count)
		{
			return (Number.Length == count);
		}

		private bool FirstDigitIsOne(string Number)
		{
			return (Number.StartsWith("1"));
		}

		private string RemoveFirst(string Number)
		{
			return Number.Remove(0,1);
		}

		private string invalidate(string Number)
		{
			return "0000000000";
		}	
	
		public new string ToString()
		{
			return "(" + Number.Substring(0,3) + ") " + Number.Substring(3,3) + "-" + Number.Substring(6,4);
		}


	}
}

